<?php

namespace App\Http\Controllers;

use App\Curso;
use Illuminate\Http\Request;

class PagesController extends Controller
{
  function home()
  {
      return view('home');
  }

  function cursos()
  {
    $cursos = Curso::all();
    return view('cursos.show', compact('cursos'));
  }

  function contactos()
  {
      return view('contactos');
  }

  function inscrever()
  {
      $cursos = Curso::all();
      return view('insc_curso', compact('cursos'));
  }
}
