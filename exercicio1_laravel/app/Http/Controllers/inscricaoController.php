<?php

namespace App\Http\Controllers;

use App\inscricao;
use Illuminate\Http\Request;

class inscricaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $incrito = inscricao::all();
      return view('inscrever.index', compact('inscrito'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('inscrever.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
          'nome' => 'required',
          'email' => 'required',
          'telemovel' => 'required|numeric'
      ]);

      $inscrito = new inscricao();
      $inscrito->nome = $request->get('nome');
      $inscrito->email = $request->get('email');
      $inscrito->telemovel = $request->get('telemovel');
      $inscrito->dataNascimento = $request->get('dataNascimento');
      $inscrito->descricao = $request->get('descricao');
      $inscrito->save();

      return redirect('/cursos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $inscrito = inscricao::find($id);
      return view('inscrever.show', compact('inscrito'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $inscrito = inscricao::find($id);
      return view('inscrever.edit')
          ->with(compact('inscrito'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
          'nome' => 'required',
          'email' => 'required',
          'telemovel' => 'required|numeric'
      ]);

      $inscrito = new inscricao();
      $inscrito->nome = $request->get('nome');
      $inscrito->email = $request->get('email');
      $inscrito->telemovel = $request->get('telemovel');
      $inscrito->dataNascimento = $request->get('dataNascimento');
      $inscrito->descricao = $request->get('descricao');
      $inscrito->save();

      return redirect('/cursos/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $inscrito = inscricao::find($id);
      $inscrito->delete();
      return redirect('/cursos');
    }
}
