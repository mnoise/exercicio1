<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscricao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('inscricao', function (Blueprint $table) {
          $table->increments('id');
          $table->increments('id_curso')->unsigned();
          $table->string('nome',100);
          $table->string('email')->unique();
          $table->string('telemovel',9);
          $table->string('descricao',100);
          $table->date('data_nascimento');
          $table->timestamps();
      });

      Schema::create('inscricao', function (Blueprint $table){
        $table->foreign('id_curso')->references('id')->on('curso');
      }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('inscricao', function (Blueprint $table){
        $table->dropforeign('id_curso_curso_foreign');
      }
        Schema::dropIfExists('inscricao');

    }
}
