@extends('layout.master')

@section('content')

<h1>Cursos</h1>
<table>
<tr>
<th>Nome</th><th>Data Inicio</th><th>Certificacao</th><th>Conteudo</th>
</tr>
@foreach ($cursos as $curso)
<tr>
  <td>{{ $curso->nome }}</td>
  <td>{{ $curso->data_inicio }}</td>
  <td>{{ $curso->certificacao }}</td>
  <td>{{ $curso->conteudo }}</td>
</tr>
@endforeach

</table>

@stop
