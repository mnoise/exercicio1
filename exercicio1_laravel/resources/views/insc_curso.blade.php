@extends('layout.master')

@section('content')

<form action="/inscricao.php" method="post">
    {{ csrf_field() }}
  <fieldset>
    <legend>Inscricao:</legend>
    <b>Nome:</b><br>
    <input type="text" name="nome"><br>
    <b>Email:</b><br>
    <input type="text" name="email"><br>
    <b>Telemovel:</b><br>
    <input type="text" name="telemovel"><br>
    <b>Data-Nascimento:</b><br>
    <input type="text" name="dataNascimento"><br>
    <b>Descriçao:</b><br>
    <input type="text" name="descricao"><br>
    <h1>Cursos:</h1>
    <select>
      @foreach ($cursos as $curso)
      <<option value="curso">{{$curso->nome}}</option>
      @endforeach
    </select>
    <input type="submit" value="Finalizar">
  </fieldset>
</form>

@stop
