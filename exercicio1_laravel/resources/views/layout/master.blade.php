<html>
<head>
  <title>
      Exercicio 1
  </title>
  <style>
  ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
      overflow: hidden;
      background-color: #333;
  }

  li {
      float: left;
  }

  li a {
      display: block;
      color: white;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
  }

  /* Change the link color to #111 (black) on hover */
  li a:hover {
      background-color: #111;
  }
  </style>
</head>
<body>
  <nav>
    <ul>
      <li><a href="/">Inicio</a></li>
      <li><a href="/cursos">Cursos</a></li>
      <li><a href="/contactos">Contactos</a></li>
      <li><a href="/inscrever">Inscrever</a></li>
    </ul>
  </nav>

  <div class="content">
    @yield('content')
  </div>
  <footer>
    <small>&copy; Copyright 2017, Marco Ferreira</small>
  </footer>

</body>
</html>
