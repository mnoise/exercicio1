<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@home');
Route::get('/cursos', 'PagesController@cursos');
Route::get('/contactos', 'PagesController@contactos');
Route::get('/inscrever', 'PagesController@inscrever');
//Route::get('/cursos', 'cursoController');
